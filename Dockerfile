FROM python:3-alpine
LABEL maintainer="eugeniyost@gmail.com"
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
COPY requirements.txt /code/
RUN apk --update add --virtual build-dependencies libffi-dev openssl-dev python-dev py-pip build-base \
  && pip install --upgrade pip \
  && pip install -r /code/requirements.txt \
  && apk del build-dependencies
COPY . /code/
EXPOSE 8000
WORKDIR /code/ci
CMD daphne -b 0.0.0.0 ci.asgi:application

# docker run --rm -p 8000:8000 -v ${PWD}:/code --name django-app django-app