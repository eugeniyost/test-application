from django.test import TestCase, Client


class SimpleTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_hello(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b"hello!")